# git_training

1. Created one dummy project in GITLAB
2. Cloned the reporsitory into the local system which gave the master branch
3. Added one file in master branch and pushed it to the GITLAB
4. Create one new feature branch in local system and pushed it to the GITLAB
5. Created one new file in the feature branch and pushed it to the GITLAB
6. Once the above step is done then raised one MERGE REQUEST from the GITLAB to merge the feature branch into the master branch
7. On success, again pulled the master branch from the repo into the local system which updated the local system project with the new file added in the feature branch
8. All the steps completed using the CLI other than the merge request.
